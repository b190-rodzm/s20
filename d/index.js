// // while
// let count = 5;
// while (count!==0){
//   console.log("while loop result:"+count);
//   count--;
// };

// count = 0;
// while (count<=10){
//     console.log("while loop result:"+count);
//     count++;
// };

// let number = Number(prompt("Give me a number"));
// do{
//   console.log("do while loop:" +number);
//   number+=1;
// }while (number <10);

let myName = "michael";

for (let x=0; x<myName.length; x++){
  if (myName[x].toLowerCase() == "a" || 
  myName[x].toLowerCase() == "e" || 
  myName[x].toLowerCase() == "i" || 
  myName[x].toLowerCase() == "o" || 
  myName[x].toLowerCase() == "u"
  ){
    console.log("*");
} else {
  console.log(myName[x].toLowerCase());
}}
  