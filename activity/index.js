let userNumber = (prompt("Please enter a number."));
console.log("The number you provided is "+userNumber+".");

for (let count = userNumber; count >= 0; count--){
  if (count <= 50) {
    console.log("Terminating the loop.");
    break;
  };
  if (count%10 === 0) {
    console.log("The number is divisible by 10. Skipping the number.");
    continue;
  }
  if (count%5 === 0){
    console.log(count);
    continue;
  };
  };


let text = "supercalifragilisticexpialidocious";
console.log(text);
let result = "";

for (let i = 0; i < text.length; i++){
  if (text[i] == "a" || text[i] == "e" || text[i] == "i" || text[i] == "o" || text[i] == "u"){
  continue;
  } else {
  result = result += text[i];
  }
}
console.log(result);
